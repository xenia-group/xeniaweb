
function vertical() {return(window.innerWidth < 1242)}

intro_hor = `                                                         
                                                           . //      contact@xenialinux.com     
                                                       //// // /     --------------------   
                                                   /// ////,/ /      OS: Xenia Linux
 @/////////                 // ****** ****,     ////// /// / /       Resolution: ${window.innerWidth}x${window.innerHeight}
   /////////////// //////////* ***** **** ** ** ////// //./,/        Blog: https://blog.xenialinux.com 
   , , //////////// ////////* ********* ** ** // ///// // //         Download: https://gitlab.com/xenia-group/xenia-linux
  ,,,  ,,, ///////// //////** ********** **** //// /// / //          Repository: https://repo.xenialinux.com 
   , ,,, ,,,, /////// ////** ******* * ******//////  / //            Wiki: https://wiki.xenialinux.com
     ,, ,, ,, ,,, ////./****,**.** **********////////                   
        ., ,, ,,,,, , /.*** ** **,********** ////////                   
            ,.,.,,,,,,, ***** * ************ //////  /                  
               ,,, ,,,,.** * ***************////// /// /                
                   ,,,,, ***,************** ////  //////                
                     ,,    **************** /// ///// //                
                      , ,,,,,,  ***********./  ////// ///               
                          ,,,,    ,,,,  ,* * //////// ///               
                                  ,,,,,,,,,, ////////*////               
                                  ,,,,  ,,,, /////// /////              
                                     ,,,,,,,  ////// ///                
                                        ,,,,,, ////////                 
                                           ,,,, /// //                  
                                             ,,, // /                   
                                                , /.                    
`

intro_vert = `
                                      **
                                 * ** */
 @/////           / *** ***   **** ***/
    //////////////**** ** * ** *** **/
  ,, ,,//////////* ****** ** ** **  /
   , , ,,, /////******* *********  /
      , ,,,,,  ** **  ***********
            ., ,,,**** ******* *** ///
             ,, *********** ** ////
               ,,,,, ******* //// //
                    ,,,,,,. ///// //
                      ,,,  , ///////
                         ,,,, /////
                            ,, / /
                                /

contact@xenialinux.com
----------------------
OS: Xenia Linux
Resolution: ${window.innerWidth}x${window.innerHeight}
Blog: https://blog.xenialinux.com
Download: https://gitlab.com/xenia-group/xenia-linux
Repository: https://repo.xenialinux.com
Wiki: https://wiki.xenialinux.com
`

about = "Xenia Linux is a Linux distribution based on Gentoo, that provides an immutable rootFS and a selection of filesystems including Btrfs, LVM, and traditional filesystems.\n\nXenia Linux uses an image-based approach to immutablity which allows for upgrades and installations to be done in a matter of seconds. Xenia implements Distrobox to allow for users to get their favourite CLI tools, and Flatpak for GUI apps.\n\nXenia Linux is built with the Gentoo build tool Catalyst, allowing for our users to easily make their own customised root images."

contact = "Jack:\n  jack@xenialinux.com\n  jack.xenia on Discord\n\nLuna:\n  luna@xenialinux.com\n  xenia.linux on Discord"

irc = "IRC: #xenialinux on irc.libera.chat"

function intro() {
    switch (vertical()) {
        case (true):
            return(intro_vert)
        case (false):
            return(intro_hor)
    }
}

jQuery(function($, undefined) {
    $('#term').terminal({
        neofetch: function() {
            this.echo(intro())
        },
        help: function() {
            this.echo(`--- HELP ---
General + information:            
    help: Shows this output
    about: Show about section
    neofetch: Display neofetch
    contact: Display contact information
    irc: Display IRC information

Links:
    blog: Go to blog
    repo: Go to repository
    gitlab: Go to GitLab page
`)
        },
        ls: function(arg) {
            switch (arg) {
                case ("-a"):
                    this.echo(". .. .secret blog repo gitlab")
                    break
                default:
                    this.echo("blog repo gitlab")
            }
        },
        secret: function() {
            window.location.href =("https://xenialinux.com/moment.html")
        },
        blog: function() {
            window.location.href =("https://blog.xenialinux.com/")
        },
        repo: function() {
            window.location.href =("https://repo.xenialinux.com/")
        },
        gitlab: function() {
            window.location.href =("https://gitlab.com/xenia-group/")
        },
        about: function() {
            this.echo(about)
        },
        contact: function() {
            this.echo(contact)
        },
        irc: function() {
            this.echo(irc)
        },
        hug: function() {
            this.echo("-hugs- <3")
        },
        hugs: function() {
            this.echo("-hugs- <3")
        }
    }, {
        prompt: 'xenia@xenialinux.com $ ',
        greetings: intro(),
        checkArity: false
    });
});
