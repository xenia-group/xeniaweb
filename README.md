# Xenia Linux Website

This repo contains the files and the pipelines for the Xenia Linux website to be hosted on GitLab Pages.

The website is hosted here: https://xenialinux.com

### Building

To build this website for local development, run `./src/build.sh` from the root directory. Then, open `public/index.html` in your browser.
